## Description
This is a simple application where i explore and study how to work with Three.js and React Three Fiber and build my Portfolio.
<br>


##  Project view

1.Beginning 
![](./src/assets/ScreenShot_11_3_2023_3_49_36_PM.png)

2.In the making
![](./src/assets/ScreenShot_11_8_2023_4_17_25_PM.png)
![](./src/assets/ScreenShot_11_8_2023_4_17_38_PM.png)

## Setup Project view
To get the project up and running, follow these steps:

- Go inside the `src` folder.
- Run `npm install` to restore all dependencies.
- After that, the project can be run with `npm run dev`

## Built With

[![](https://skills.thijs.gg/icons?i=js,threejs,vite,)](https://skills.thijs.gg).


## Appreciation
ABSURD 3D