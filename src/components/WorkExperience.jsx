import { Swiper, SwiperSlide } from "swiper/react";
import { EffectCube, Autoplay } from "swiper";
import "swiper/css";
import "swiper/css/effect-cube";
import "swiper/css/autoplay";

import React from "react";
const working = [
  {
    id: 0,
    jobTitle: "Job Title",
    companyName: "Company Name",
    datesWorked: " 2020 - 2023 ",
    jobDescription:
      "Put the details of the job and skills required here. Example: I worked with React Three Fiber, Framer Motion, and Swiper.js to build amazing looking UI that is reuseable. I used Node and Express to provide functional CRUD operations with a noSQL database.",
  },
  {
    id: 1,
    jobTitle: "Job Title",
    companyName: "Company Name",
    datesWorked: " 2020 - 2023 ",
    jobDescription:
      "Put the details of the job and skills required here. Example: I worked with React Three Fiber, Framer Motion, and Swiper.js to build amazing looking UI that is reuseable. I used Node and Express to provide functional CRUD operations with a noSQL database.",
  },
  {
    id: 2,
    jobTitle: "Job Title",
    companyName: "Company Name",
    datesWorked: " 2020 - 2023 ",
    jobDescription:
      "Put the details of the job and skills required here. Example: I worked with React Three Fiber, Framer Motion, and Swiper.js to build amazing looking UI that is reuseable. I used Node and Express to provide functional CRUD operations with a noSQL database.",
  },
];
const WorkExperience = () => {
  const Card = ({ jobTitle, companyName, datesWorked, jobDescription }) => {
    return (
      <div className="border-2 w-[98%] glass-background rounded-lg  flex flex-col  glowing-shadow">
        <div className="text-white flex flex-col justify-start items-center  mx-1 my-5">
          <h2 className="text-lg text-center mb-6">{jobTitle}</h2>
          <h3 className="text-lg text-center mb-6">{companyName}</h3>
          <p className="text-center mb-6">{datesWorked}</p>
          <p className="text-center mb-6">{jobDescription}</p>
        </div>
      </div>
    );
  };
  return (
    <section className="relative w-screen h-screen overflow-hidden">
      <div className="w-full h-full z-8 flex flex-col justify-center items-center text-white  my-5">
        <h1 className="text-5xl lg:text-7xl mt-10 fredoka-font text-center mx-1">
          Work
          <span className=" light-blue-text font-semibold "> Exp</span>
          erience
        </h1>
        <p className="mt-6">Swipe or drag</p>
        <div className="w-[90%] lg:w-1/4 my-8">
          <Swiper
            grabCursor={true}
            spaceBetween={0}
            slidesPerView={1}
            autoplay={{
              delay: 6000,
              disableOnInteraction: false,
            }}
            speed={2000}
            loop={true}
            effect={"cube"}
            cubeEffect={{
              shadow: true,

              slideShadows: true,
              shadowOffset: 16,

              shadowScale: 0.93,
            }}
            modules={[EffectCube, Autoplay]}
            className="mySwiper"
          >
            {working.map((e) => (
              <SwiperSlide key={e.id}>
                <Card
                  jobTitle={e.jobTitle}
                  companyName={e.companyName}
                  datesWorked={e.datesWorked}
                  jobDescription={e.jobDescription}
                />
              </SwiperSlide>
            ))}
          </Swiper>
        </div>
      </div>
    </section>
  );
};

export default WorkExperience;
