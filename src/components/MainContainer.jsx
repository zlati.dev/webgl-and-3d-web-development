import "../index.css";
import { Canvas } from "@react-three/fiber";
import ReactLogo from "./canvas/ReactLogo";

import Projects from "./Projects";
import Contact from './Contact';
import { Loader, PerspectiveCamera, Stars } from "@react-three/drei";

import { StarsAnimated } from "./StarsAnimated";
import Hero from "./Hero";
import { Suspense } from "react";
import About from "./About";
import Earth from './canvas/Earth';
import WorkExperience from "./WorkExperience";
const MainContainer = () => {
  const bgColor = ({ gl }) => {
    gl.setClearColor("#000000", 1);
  };


  return (
    <>
      <Canvas
        id="canvas"
        style={{ position: "fixed" }}
        camera={{ position: [21, 3, 6], fov: 16 }}
        onCreated={bgColor}
      >
        {/* <axesHelper /> */}
        <pointLight intensity={2} color={0x61dbfb} position={[0, 6, 6]} />
        <spotLight intensity={1} color={0x61dbfb} position={[-21, 60, 12]} />
        <StarsAnimated />
        <Suspense fallback={null}>
          <ReactLogo />
          <Earth/>
        </Suspense>
      </Canvas>
      <Loader />
      <Hero />
      <About />
      <Projects />
      <WorkExperience />
      <Contact/>
    </>
  );
};

export default MainContainer;
