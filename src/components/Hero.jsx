import React, { useEffect } from 'react'
import { motion, useAnimation } from 'framer-motion'
import { useInView } from 'react-intersection-observer'
const Hero = () => {

  const isMobile = window.innerWidth <= 767
  const delayTime = isMobile ? 1.8 : 0.6
  const h1variants = {
    visible: {
      opacity: 1,
      scale: 1,
      transition: {
        type: 'spring',
        bounce: 0.5,
        stiffness: 100,
        duration: 1,
        delay: delayTime,
      }
    },
    hidden: { opacity: 0, scale: 0 }
  }


  const h2variants = {
    visible: {
      opacity: 1,
      scale: 1,
      transition: {
        type: 'spring',
        bounce: 0.5,
        stiffness: 100,
        duration: delayTime + 0.6,
        delay: 1.6,
      }
    },
    hidden: { opacity: 0, scale: 0 }
  }


  const control = useAnimation()
  const [ref, inView] = useInView()
  useEffect(() => {
    if (inView) {
      control.start('visible')
    }
  }, [control, inView])

  return (
    <section className='w-screen h-screen mx-auto'>
      <div className='absolute left-[5%] top-[5%] xl:left-[14%] xl:top-[14%] mx-auto fredoka-font text-white'>
        <motion.h1 ref={ref} animate={control}
          initial='hidden'
          className='text-5xl lg:text-7xl' variants={h1variants}>
          I am Z
        </motion.h1>
        <motion.h2 ref={ref} animate={control}
          initial='hidden'
          className='mt-8 text-3xl w-2/3 text-center ' variants={h2variants}>
          Junior Developer
        </motion.h2>
      </div>
      <div className='absolute flex flex-col bottom-[13%] left-[50%] translate-x-[-50%]'   >
        <div className='slide-bottom'>
          <div className='down-arrow'></div>
          <div className='down-arrow'></div>
          <div className='down-arrow'></div>
        </div>
      </div>
    </section>
  )
}

export default Hero
