import { Swiper, SwiperSlide } from "swiper/react";
import { EffectCoverflow, Autoplay } from "swiper";
import web from "../assets/web.png";
import "swiper/css";
import "swiper/css/effect-coverflow";
import "swiper/css/autoplay";
const slide = [
  {
    id: 0,
    imageSrc:
      "https://gitlab.com/zlati.dev/simple-sap-application/-/raw/main/sap/src/assets/v.png",
    projectName: "Simple SAP Application",
    projectLink: "https://gitlab.com/zlati.dev/simple-sap-application",
    projectDescription: `This is a simple application that fetches some data and after that transforms and works with the provided information.`,
    gitlabLink: "https://gitlab.com/zlati.dev/simple-sap-application",
  },
  {
    id: 1,
    imageSrc: web,
    projectName: "Website for AI",
    projectLink: "https://gitlab.com/zlati.dev/website-for-ai",
    projectDescription: `This is a simple application where i start to learn Next.js and how it works,what are the things that the end-user can benefit and how to work with it .`,
    gitlabLink: "https://gitlab.com/zlati.dev/website-for-ai",
  },
  {
    id: 2,
    imageSrc:
      "https://gitlab.com/zlati.dev/webgl-and-3d-web-development/-/raw/main/src/assets/ScreenShot_11_3_2023_3_49_36_PM.png",
    projectName: "WebGl and 3D Web Development",
    projectLink: "https://gitlab.com/zlati.dev/webgl-and-3d-web-development",
    projectDescription: `This is a simple application where i explore and study how WebGl and 3D Web Development is working.`,
    gitlabLink: "https://gitlab.com/zlati.dev/webgl-and-3d-web-development",
  },
];
const Projects = () => {
  const Card = ({
    imageSrc,
    projectName,
    projectLink,
    projectDescription,
    gitlabLink,
  }) => {
    return (
      <div className=" w-full glass-background rounded-lg overflow-hidden flex flex-col">
        <div className="w-full">
          <a href={projectLink} className="block w-full h-full">
            <img
              src={imageSrc}
              alt={projectName}
              className="w-full object-cover cursor-pointer flex-shrink"
            />
          </a>
        </div>
        <div className="text-white flex flex-col justify-start items-center h-[40%] mx-1 my-1">
          <h2 className="text-center bold my-3">{projectName} </h2>
          <p className="text-center mb-6">{projectDescription}</p>
          <div className="flex justify-center mb-1">
            <a>
              <img
                src="https://img.icons8.com/material-outlined/24/61dbfb/github.png"
                alt="gitlabLink"
                className="w-8 h-8 hover-glowing-shadow-and-scale rounded-full"
              />
            </a>
          </div>
        </div>
      </div>
    );
  };

  return (
    <section className="relative w-screen h-full">
      <div className="w-full h-screen z-10 flex flex-col justify-center items-center text-white ">
        <h1 className="text-5xl lg:text-7xl mt-10 fredoka-font text-center">
          Portfolio
          <span className=" light-blue-text font-semibold "> Pro</span>
          jects{" "}
        </h1>
        <p className="mt-5">Swipe or drag the items</p>
        <div className="w-full h-screen sm:w-3/4 lg:w-1/2 xl:w-1/3 my-10 ">
          <Swiper
            grabCursor={true}
            centeredSlides={true}
            // Responsive breakpoints
            breakpoints={{
              // when window width is >=
              1: {
                slidesPerView: 1.3,
                spaceBetween: 120,
              },
              400: {
                slidesPerView: 1.3,
                spaceBetween: 200,
              },
              1024: {
                slidesPerView: 1.3,
                spaceBetween: 175,
              },
              1280: {
                slidesPerView: 1.5,
                spaceBetween: 150,
              },
              1580: {
                slidesPerView: 1.5,
                spaceBetween: 175,
              },
            }}
            autoplay={{
              delay: 5000,
              disableOnInteraction: false,
            }}
            speed={2000}
            loop={true}
            effect={"coverflow"}
            coverflowEffect={{
              rotate: 50,
              stretch: 0,
              depth: 200,
              modifier: 3,
              slideShadows: true,
            }}
            autoHeight={true}
            resizeObserver={true}
            modules={[EffectCoverflow, Autoplay]}
          >
            {slide.map((s) => (
              <SwiperSlide key={s.id}>
                <Card
                  imageSrc={s.imageSrc}
                  projectName={s.projectName}
                  projectLink={s.projectLink}
                  projectDescription={s.projectDescription}
                  gitlabLink={s.gitlabLink}
                />
              </SwiperSlide>
            ))}
          </Swiper>
        </div>
      </div>
      <div className='absolute flex flex-col bottom-[13%] left-[50%] translate-x-[-50%]'   >
        <div className='slide-bottom'>
          <div className='down-arrow'></div>
          <div className='down-arrow'></div>
          <div className='down-arrow'></div>
        </div>
      </div>
    </section>
  );
};
export default Projects;
