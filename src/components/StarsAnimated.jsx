import { Stars } from "@react-three/drei";
import { useFrame } from "@react-three/fiber";
import { useMemo, useRef } from "react";

export const StarsAnimated = () => {
  const starsRef = useRef();
  const staProp = useMemo(
    () => ({
      radius: 60,
      depth: 30,
      count: 6000,
      factor: 6,
      saturation: 1,
      fade: true,
      speed: 1,
    }),


    []
  );

  useFrame(() => {
    starsRef.current.rotation.y += 0.0001;

    starsRef.current.rotation.x += 0.0001;
    starsRef.current.rotation.z += 0.0001;
  });
  return <Stars ref={starsRef} {...staProp} />;
};
