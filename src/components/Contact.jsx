import { useInView } from "react-intersection-observer";
import { motion, useAnimation } from "framer-motion";
import emailjs from "@emailjs/browser";

import { useRef, useState, useEffect } from "react";
const Contact = () => {
  const formRef = useRef();
  const [form, setThe] = useState({
    name: "",
    email: "",
    message: "",
  });
  const [reg, setReg] = useState(false);

  const handleChange = (e) => {
    const { target } = e;
    const { name, value } = target;
    setThe({
      ...form,
      [name]: value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setReg(true);

    emailjs
      .send(
        "service_1234567",
        "template_1234567",
        {
          from_name: form.name,
          to_name: "Erik",
          from_email: form.email,
          to_email: "random@gmail.com",
          message: form.message,
        },
        "anotherAuth"
      )
      .then(
        () => {
          setLoading(false);
          alert("Thanks! I will get back to you soon.");

          setThe({
            name: "",
            email: "",
            message: "",
          });
        },
        (error) => {
          setLoading(false);
          console.error(error);

          alert("Oops, something went wrong. Please try again.");
        }
      );
  };
  const contactVariants = {
    visible: {
      opacity: 1,
      scale: 1,
      transition: {
        duration: 1,
      },
    },
    hidden: { opacity: 0, scale: 0 },
  };

  const controls = useAnimation();
  const [ref, inView] = useInView();
  useEffect(() => {
    if (inView) {
      controls.start("visible");
    }
  }, [controls, inView]);

  return (
    <section className="w-full h-full relative flex flex-col justify-center items-center overflow-hidden">
      <motion.div
        ref={ref}
        animate={controls}
        initial="hidden"
        variants={contactVariants}
        className=" border-3  w-[98%] md:w-[60%] xl:w-[38%] text-center transparent-glass-background py-8 px-3 rounded-3xl glowing-shadow mt-16 mb-8 "
      >
        <h1 className=" header-text-glow fredoka-font text-5xl lg:text-7xl text-white ">
          C<span className=" light-blue-text font-semibold ">on</span>t
          <span className=" light-blue-text font-semibold ">act</span>
        </h1>
        <form
          ref={formRef}
          onSubmit={handleSubmit}
          className="   mt-12 flex flex-col gap-8"
        >
          <label className="flex flex-col"  >
            <span className="   text-white mb-3 font-bold">Your Name</span>
            <input
          
              type="text"
              name="name"
              value={form.value}
              onChange={handleChange}
              placeholder="What`s your name"
              className="border-3form-field-color py-4 px-6 text-color='#171769'
            rounded-lg outline-none  font-medium "
            />
          </label>
          <label className="flex flex-col">
            <span className="   text-white mb-3 font-bold">Your Ema</span>
            <input
              type="email"
              name="email"
              value={form.email}
              onChange={handleChange}
              placeholder="What`s your email"
              className="border-3form-field-color py-4 px-6 text-color='#171769'
            rounded-lg outline-none  font-medium "
            />
          </label>
          <label className="flex flex-col">
            <span className="   text-white mb-3 font-bold">Your tex</span>

            <textarea
              name="message"
              value={form.message}
              onChange={handleChange}
              placeholder="What`s your message"
              className="border-3form-field-color py-4 px-6 text-color='#171769'
  rounded-lg outline-none  font-medium "
            ></textarea>
          </label>
          <button
            type="submit"
            className=" w-79 h-6 hover-glowing-shadow-and-scale rounded-full text-white"
          >
            {reg ? "Submitting" : "Enter"}
          </button>
        </form>
      </motion.div>
    </section>
  );
};

export default Contact;
