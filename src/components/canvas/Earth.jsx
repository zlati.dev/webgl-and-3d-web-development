import React, { useCallback, useMemo, useState } from 'react'
import { useEffect, } from 'react'





import { useGLTF, useAnimations } from "@react-three/drei";
const actionName = ['Base Stack']
const Earth = () => {

  const [scale, setScale] = useState(0.6)
  const memoEarth = useMemo(() => {
    return useGLTF('../../public/earth/scene.gltf')
  })
  const animation = useAnimations(
    memoEarth.animations,
    memoEarth.scene
  )
  const onScroll = useCallback(() => {
    const percentage = window.scrollY / (document.body.scrollHeight - window.innerHeight)
    const newScale = 0.6 + 3 * percentage // adjust the scaling factor as desired
    setScale(newScale)
  })
  useEffect(() => {
    actionName.forEach((action) => {

      const actionToBe = animation.actions[action]
      actionToBe.play()

    })
    window.addEventListener('scroll', onScroll)
    return () => (
      window.removeEventListener('scroll', onScroll)
    )
  }, [animation, onScroll])
  return (
    <mesh>
      <primitive
        object={memoEarth.scene} scale={scale} />

    </mesh>
  )


}

export default Earth